#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright 2005-2016 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version
 * 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
package ${package}.${artefactIdLower}.routes;

import javax.xml.bind.UnmarshalException;
import javax.xml.bind.MarshalException;
import org.apache.camel.ValidationException;
#if(${incomingTypeSupport} == "rest")
import org.apache.camel.http.common.HttpOperationFailedException;
#end
import javax.xml.xpath.XPathExpressionException;
#if(${incomingTypeSupport} == "sftp")
import java.io.IOException;
#end

import org.apache.camel.component.bean.validator.BeanValidationException;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
#if(${incomingTypeSupport} == "rest")
import org.springframework.beans.factory.annotation.Value;
#end
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import org.apache.camel.LoggingLevel;

import ${package}.${artefactIdLower}.configurator.ConfigurationRoute;
import ${package}.${artefactIdLower}.properties.MailProperties;
import ${package}.${artefactIdLower}.transformations.FailureErrorProcessor;
import ${package}.${artefactIdLower}.model.Order;

@Component
public class TransformationRoute extends ConfigurationRoute {

	@Autowired
	private MailProperties mailConfig;
	
	#if(${incomingTypeSupport} == "rest")
@Value("${headersValidationService}")
	private String headersValidationService;
	#end
	
	public void configure() throws Exception {
		super.configure();
		
		JacksonDataFormat format = new JacksonDataFormat(Order.class);		
		#if(${incomingTypeSupport} == "rest")
onException(HttpOperationFailedException.class).handled(true)
			.log(LoggingLevel.ERROR, "WS-16 La ruta no logra resolver el endpoint externo de validacion, presenta el codigo de error 404 en la ruta ${routeId}") 
			.log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
			.log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
			.log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
			.setHeader("mailErrorDescription", simple(mailConfig.getErrorEstructura().toString()))
			.to("direct:mailNotification")
		.end();
		#end
		
		onException(NullPointerException.class).handled(true)
	        .log(LoggingLevel.ERROR, "TRV-02 La estructura del mensaje a procesar presenta errores en la ruta ${routeId}")
	        .log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
	        .log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
	        .log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
	        .setHeader("mailErrorDescription", simple(mailConfig.getErrorEstructura().toString()))
	        .to("direct:mailNotification")
        .end();
		
		onException(InvalidFormatException.class).handled(true)
	        .log(LoggingLevel.ERROR, "TRV-03 Exception de formato, error de serializacion o deserializacion en la ruta ${routeId}")
	        .log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
	        .log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
	        .log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
	        .setHeader("mailErrorDescription", simple(mailConfig.getErrorEstructura().toString()))
	        .to("direct:mailNotification")
        .end();
		
		onException(UnmarshalException.class).handled(true)
	        .log(LoggingLevel.ERROR, "WS-03 La estructura del mensaje a procesar presenta errores en la ruta ${routeId}")
	        .log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
	        .log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
	        .log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
	        .setHeader("mailErrorDescription", simple(mailConfig.getErrorEstructura().toString()))
	        #if(${incomingTypeSupport} == "rest")
.setHeader("CamelHttpResponseHeader", simple("400"))
#end
	        .to("direct:mailNotification")
        .end();
		
		onException(JsonParseException.class).handled(true)
	        .log(LoggingLevel.ERROR, "WS-06 El contenido del mensaje no se ajusta a la sintaxis json, error en la ruta ${routeId}")
	        .log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
	        .log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
	        .log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
	        .setHeader("mailErrorDescription", simple(mailConfig.getErrorEstructura().toString()))
	        #if(${incomingTypeSupport} == "rest")
.setHeader("CamelHttpResponseHeader", simple("400"))
#end
	        .to("direct:mailNotification")
        .end();
		
		onException(MarshalException.class).handled(true)
	        .log(LoggingLevel.ERROR, "WS-10 La estructura del mensaje a procesar presenta errores en la ruta ${routeId}")
	        .log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
	        .log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
	        .log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
	        .setHeader("mailErrorDescription", simple(mailConfig.getErrorEstructura().toString()))
	        #if(${incomingTypeSupport} == "rest")
.setHeader("CamelHttpResponseHeader", simple("400"))
#end
	        .to("direct:mailNotification")
        .end();
		
		onException(BeanValidationException.class).handled(true)
	        .log(LoggingLevel.ERROR, "BNS-01 La estructura del mensaje entrante presenta errores en la ruta ${routeId}")
	        .log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
	        .log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
	        .log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
	        .setHeader("mailErrorDescription", simple(mailConfig.getErrorEstructura().toString()))
	        #if(${incomingTypeSupport} == "rest")
.setHeader("CamelHttpResponseHeader", simple("400"))
#end
	        .to("direct:mailNotification")
        .end();
		
		onException(ValidationException.class).handled(true)
	        .log(LoggingLevel.ERROR, "BNS-02 La estructura del mensaje entrante presenta errores en la ruta ${routeId}")
	        .log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
	        .log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
	        .log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
	        .setHeader("mailErrorDescription", simple(mailConfig.getErrorEstructura().toString()))
	        #if(${incomingTypeSupport} == "rest")
.setHeader("CamelHttpResponseHeader", simple("400"))
#end
	        .to("direct:mailNotification")
        .end();
		
		onException(XPathExpressionException.class).handled(true)
	        .log(LoggingLevel.ERROR, "BNS-03 La expresión entregada no se pudo aplicar al mensaje y presenta errores en la ruta ${routeId}")
	        .log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
	        .log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
	        .log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
	        .setHeader("mailErrorDescription", simple(mailConfig.getErrorEstructura().toString()))
	        .to("direct:mailNotification")
	    .end();
		
		#if(${incomingTypeSupport} == "sftp")
onException(IOException.class).handled(true)
	        .log(LoggingLevel.ERROR, "BNS-04 Error en la interpretacion del archivo y presenta errores en la ruta ${routeId}")
	        .log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
	        .log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
	        .log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
	        .setHeader("mailErrorDescription", simple(mailConfig.getErrorEstructura().toString()))
	        .to("direct:mailNotification")
	    .end();
		#end
		
		//Here is where you need make all the process in the route.
		from("direct:transformationRoute").routeId("${artefactIdLower}_transformation")
		#if(${incomingTypeSupport} == "rest")
	.removeHeaders("CamelHttp*", "SCanal","SUsuario","SOrigenCliente")
			.setHeader("CamelHttpMethod", constant("POST"))
			.to("http4://"+headersValidationService)
			.choice()
				.when()
					.simple("${headers.successValidation} == 'true'")
					.log("continue...")
				.otherwise()
					.throwException(UnmarshalException.class, "Por favor verificar los cabeceros de seguridad de la peticion.")
			.end()#end
			
			.unmarshal(format)
			.to("bean-validator://x")
			.bean("transformationComponent", "transformation")
			.marshal(format)//.json(JsonLibrary.Jackson, Order.class)
			.log("Sending to producer")
#if(${outputTypeSupport} == "amq")	
#parse ( "producer/ProducerAMQ.txt")
#end
#if(${outputTypeSupport} == "sftp" || ${outputTypeSupport} == "ftp")	
#parse ( "producer/ProducerSFTP.txt")
#end
#if(${outputTypeSupport} == "rest")	
#parse ( "producer/ProducerRest.txt")
#end
#if(${outputTypeSupport} == "sql")	
#parse ( "producer/ProducerSQL.txt")
#end
#if(${outputTypeSupport} == "batis")	
#parse ( "producer/ProducerBatis.txt")
#end
#if(${outputTypeSupport} == "soap")	
#parse ( "producer/ProducerSOAP.txt")
#end
#if(${outputTypeSupport} == "smtp")	
#parse ( "producer/ProducerSMTP.txt")
#end

		.end();
		
		from("direct:mailNotification").routeId("${artefactIdLower}_amq_gotonotification")
			.bean("createMail", "crearMail")
			.log("Enviando a direct:amqNotificationProducerRoute")
			.to("direct:amqNotificationProducerRoute")
		.end();
	}
}