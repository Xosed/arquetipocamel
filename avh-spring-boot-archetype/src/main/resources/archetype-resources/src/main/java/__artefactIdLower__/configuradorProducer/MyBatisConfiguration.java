#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright 2005-2016 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version
 * 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
package ${package}.${artefactIdLower}.configurator;

import org.apache.camel.component.mybatis.MyBatisComponent;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import ${package}.${artefactIdLower}.properties.BatisProducer;

@Configuration
public class MyBatisConfiguration {
	
	@Value("${mybatis.mapper-locations}")
    private String variable;

    @Autowired
    private DatasourceConfigurationProducer datasourceProducerInterface;

	@Autowired
    private BatisProducer configuracionBatisInterface;
	
	@Autowired
	private ResourceLoader resourceLoader;

	@Bean(name="mybatis")
	public MyBatisComponent myBatisComponentInterface( ) throws Exception {
        
		SqlSessionFactoryBean fabricaConnection = new SqlSessionFactoryBean();
        fabricaConnection.setDataSource(datasourceProducerInterface.getConfig());
        Resource recurso = resourceLoader.getResource(variable);
        Resource[] resources = new Resource[1];
        resources[0] = recurso;
        fabricaConnection.setMapperLocations(resources);
        SqlSessionFactory sqlSessionFactory = fabricaConnection.getObject();
        MyBatisComponent component = new MyBatisComponent();
		component.setSqlSessionFactory( sqlSessionFactory );
		return component;
	}
}