#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright 2005-2016 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version
 * 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
package ${package}.${artefactIdLower}.routes;

import java.net.ConnectException;

import org.apache.camel.LoggingLevel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ${package}.${artefactIdLower}.properties.SmtpProducer;
import ${package}.${artefactIdLower}.configurator.ConfigurationRoute;
import ${package}.${artefactIdLower}.properties.MailProperties;

@Component
public class SmtpProducerRoute extends ConfigurationRoute  {
	
	@Autowired
	private SmtpProducer smtpProducerBase;
	
	@Autowired
	private MailProperties mailConfig;

	public void configure() throws Exception {
		super.configure();
		
		onException(ConnectException.class).handled(true)
		    .maximumRedeliveries(3)
		    .redeliveryDelay(2000)
		    .log(LoggingLevel.ERROR, "TRV-01 El host de destino no ha sido alcanzado presenta errores de conexi�n en la ruta ${routeId}")
		    .log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
		    .log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
		    .log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
		    .setHeader("mailErrorDescription", simple(mailConfig.getErrorConexion().toString()))
		    .to("direct:mailNotification")
	    .end();
		
		from("direct:sendMail").id("${artefactIdLower}_send_mail_route")
			.log("Enviando mensaje...")
			.to("smtp://sendMail?"
					+ "host="+smtpProducerBase.getHost()
					+ "&port="+smtpProducerBase.getPort()
					+ "&contentType=text/html"
					+ "&password="+smtpProducerBase.getPassword()
					+ "&username="+smtpProducerBase.getUser()
					+ "&mail.smtp.starttls.enable="+smtpProducerBase.isSmtpStarttls()
					+ "&mail.smtp.auth="+smtpProducerBase.isSmtpAuth())
			.log("Proceso de env�o finalizado.")
		.end();
		
		from("direct:smtpProducerRoute").id("${artefactIdLower}_smtp_producer")
			.setHeader("from", simple(smtpProducerBase.getFrom()))
			.setHeader("to", simple(smtpProducerBase.getTo()))
			.setHeader("subject", simple(smtpProducerBase.getSubject()))
			.setHeader("template", simple(smtpProducerBase.getTemplate()))
			.setHeader("cc", simple(smtpProducerBase.getCc()))
			.setHeader("bcc", simple(smtpProducerBase.getBcc()))
			//Ajuste de adjuntos
			.log("Ingreso a verificaci�n de adjuntos.")
			//This option is disabled, you can enable it when need to check by attach files.
			//.bean("attachmentsProcessor", "ejecutar")
			.log("Ingreso a Carga de plantillas.")
			//Carga de templates
			.bean("cargaPlantillas", "loadTemplate")
			.log("Construyendo mensaje con velocity: ${headers.template}.")
			.to("velocity:CamelVelocityTemplate")
			.removeHeaders("*", "from|to|subject|cc|bcc|template")
			.log("Sending to producer: \n ${body}")
			.to("direct:sendMail")
			.log("Proceso de env�o finalizado.")
		.end();
	}
}
