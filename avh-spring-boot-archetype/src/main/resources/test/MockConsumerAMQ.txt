		final RouteDefinition routeConsumer = context.getRouteDefinition("${artefactIdLower}_amqp_consumer");
		routeConsumer.adviceWith(context, new AdviceWithRouteBuilder() {
			@Override
			public void configure() throws Exception {
				replaceFromWith("direct:amqConsumerRoute");
			}
		});