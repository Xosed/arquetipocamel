#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright 2005-2016 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version
 * 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
package ${package}.${artefactIdLower}.routes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ${package}.${artefactIdLower}.properties.SftpConsumer;
import ${package}.${artefactIdLower}.configurator.ConfigurationRoute;

@Component
public class SftpConsumerRoute extends ConfigurationRoute {

	@Autowired
	private SftpConsumer sftpConfig;


	public void configure() throws Exception {
		super.configure();
		
		StringBuilder path = new StringBuilder();
		path.append(sftpConfig.getHostName()
			+ ":"
			+ sftpConfig.getPort()
			+ "/"
			+ sftpConfig.getPathDownload()
			+ "?password=RAW("
			+ sftpConfig.getPasswd()
			+ ")&username="
			+ sftpConfig.getUser()
			+ "&jschLoggingLevel="
			+ sftpConfig.getJschLoggingLevel()
			+ "&throwExceptionOnConnectFailed=true&consumer.pollStrategy=#sftpPollingConsumerPollStrategy"
			+ "&maximumReconnectAttempts="
			+ sftpConfig.getReconnectAttempts()
			);
		
		if (sftpConfig.getProcessedDirectory() != null) {
			if (!sftpConfig.getProcessedDirectory().trim().isEmpty()) {
				path.append("&move="+ sftpConfig.getProcessedDirectory());
			}
		}
		if (sftpConfig.getErrorDirectory() != null) {
			if (!sftpConfig.getErrorDirectory().trim().isEmpty()) {
				path.append("&moveFailed=" + sftpConfig.getErrorDirectory());
			}
		}
		if (sftpConfig.getRegexFileName() != null) {
			if (!sftpConfig.getRegexFileName().trim().isEmpty()) {
				path.append("&include=" + sftpConfig.getRegexFileName());
			}
		}
		if (sftpConfig.getAutoCreate() != null) {
			if (!sftpConfig.getAutoCreate().trim().isEmpty()) {
				path.append("&autoCreate=" + sftpConfig.getAutoCreate());
			}
		}
		
		from("sftp://" + path.toString()).routeId("${artefactIdLower}_ftp_consumer")
			.choice()
				.when(header("attachmentName").isEqualTo("testFile"))
					.to("direct:transformationRoute")
				.when(header("").isEqualTo("something"))
					.to("mock:destination1")
			.otherwise().to("mock:destination2").end();
	}
}
