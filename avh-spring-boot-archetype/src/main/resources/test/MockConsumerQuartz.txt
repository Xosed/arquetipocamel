		final RouteDefinition routeConsumer = context.getRouteDefinition("${artefactIdLower}_quartz2_consumer");
		routeConsumer.adviceWith(context, new AdviceWithRouteBuilder() {
			@Override
			public void configure() throws Exception {
				replaceFromWith("direct:quartz2ConsumerRoute");
			}
		});		