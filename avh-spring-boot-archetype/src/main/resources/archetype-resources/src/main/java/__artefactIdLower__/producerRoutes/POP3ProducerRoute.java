#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright 2005-2016 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version
 * 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
package ${package}.${artefactIdLower}.routes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.apache.camel.LoggingLevel;

import java.net.ConnectException;

import ${package}.${artefactIdLower}.properties.POP3Producer;
import ${package}.${artefactIdLower}.transformations.AttachmentsExpression;
import ${package}.${artefactIdLower}.transformations.MailProcessor;
import ${package}.${artefactIdLower}.configurator.ConfigurationRoute;
import ${package}.${artefactIdLower}.properties.MailProperties;

@Component
public class POP3ProducerRoute extends ConfigurationRoute  {
	
	@Autowired
	private POP3Producer pop3Config;
	
	@Autowired
	private MailProperties mailConfig;

	public void configure() throws Exception {  
		super.configure();
		
		onException(ConnectException.class).handled(true)
		    .maximumRedeliveries(3)
		    .redeliveryDelay(2000)
		    .log(LoggingLevel.ERROR, "TRV-01 El host de destino no ha sido alcanzado presenta errores de conexi�n en la ruta ${routeId}")
		    .log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
		    .log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
		    .log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
		    .setHeader("mailErrorDescription", simple(mailConfig.getErrorConexion().toString()))
		    .to("direct:mailNotification")
	    .end();
//        context().setStreamCaching(true);  
		String path=
			pop3Config.getUser() +
			"@" +
			pop3Config.getDomain() +
			"?password=" +
			pop3Config.getPasswd() +
			"&consumer.delay=" +
			pop3Config.getDelay();
        from("direct:pop3ProducerRoute").routeId("${artefactIdLower}_pop3_producer")
            .to("pop3://" + path) 
        .end();  
      }  
}
