import java.util.regex.Pattern
import org.apache.commons.io.FileUtils
import groovy.io.FileType

def pack = new String(request.getProperties().get("package")).replaceAll("\\.","\\/")
def ruta = "/src/main/java/"+pack
def rutaResources = request.getOutputDirectory() + "/" + request.getArtifactId() + "/src/main/resources/"
def rutaResourcesTest = request.getOutputDirectory() + "/" + request.getArtifactId() + "/src/test/resources/"
def rutaArchivos = request.getOutputDirectory() + "/" + request.getArtifactId() + "/src/main/archivos/"
def nombreArchivo = request.getOutputDirectory() + "/" + request.getArtifactId()+ruta
def finalRoot = nombreArchivo + "/"+request.getProperties().get("artefactIdLower")+"/"
//print finalRoot
def optionPersistence = request.getProperties().get("incomingTypeSupport")
def optionOutput = request.getProperties().get("outputTypeSupport")
def folderPath = finalRoot
def varVerificar = optionPersistence.toLowerCase()
def varOutput = optionOutput.toLowerCase()
//Persistence
//Verify the input type supported.
if (varVerificar == 'amq' ) {
	new File(rutaArchivos+"amq_base.properties").renameTo(rutaResources + "amq_base.properties")
	new File(folderPath+"/configuradorConsumer/AmqConfigurationConsumer.java").renameTo(folderPath+"/configurator/AmqConfigurationConsumer.java")
	new File(folderPath+"/consumerProperties/AmqConsumer.java").renameTo(folderPath+"/properties/AmqConsumer.java")
	new File(folderPath+"/consumerRoutes/AMQPConsumerRoute.java").renameTo(folderPath+"/routes/AMQPConsumerRoute.java")
	print rutaArchivos
}

if (varVerificar == 'sql' ) {
	new File(rutaArchivos+"database_base.properties").renameTo(rutaResources + "database_base.properties")
	new File(folderPath+"/configuradorConsumer/DatasourceConfigurationConsumer.java").renameTo(folderPath+"/configurator/DatasourceConfigurationConsumer.java")
	new File(folderPath+"/consumerProperties/DatasourceConsumer.java").renameTo(folderPath+"/properties/DatasourceConsumer.java")
	new File(folderPath+"/consumerRoutes/DatasourceConsumerRoute.java").renameTo(folderPath+"/routes/DatasourceConsumerRoute.java")
	print rutaArchivos
}

if (varVerificar == 'sftp' || varVerificar == 'ftp') {
	new File(rutaArchivos+"sftp_base.properties").renameTo(rutaResources + "sftp_base.properties")
	new File(folderPath+"/consumerProperties/SftpConsumer.java").renameTo(folderPath+"/properties/SftpConsumer.java")
	new File(folderPath+"/consumerRoutes/SftpConsumerRoute.java").renameTo(folderPath+"/routes/SftpConsumerRoute.java")
	new File(folderPath+"/sftpPollStrategyListener/SftpPollingConsumerPollStrategy.java").renameTo(folderPath+"/transformations/SftpPollingConsumerPollStrategy.java")
	print rutaArchivos
}

if (varVerificar == 'rest' ) {
	new File(rutaArchivos+"rest_base.properties").renameTo(rutaResources + "rest_base.properties")
	new File(folderPath+"/consumerProperties/RestConsumer.java").renameTo(folderPath+"/properties/RestConsumer.java")
	new File(folderPath+"/consumerRoutes/RestConsumerRoute.java").renameTo(folderPath+"/routes/RestConsumerRoute.java")
	print rutaArchivos
}

if (varVerificar == 'pop3' ) {
	new File(rutaArchivos+"pop3_base.properties").renameTo(rutaResources + "pop3_base.properties")
	new File(folderPath+"/consumerProperties/POP3Consumer.java").renameTo(folderPath+"/properties/POP3Consumer.java")
	new File(folderPath+"/consumerRoutes/POP3ConsumerRoute.java").renameTo(folderPath+"/routes/POP3ConsumerRoute.java")
}

if (varVerificar == 'quartz' ) {
	new File(rutaArchivos+"quartz.properties").renameTo(rutaResources + "quartz.properties")
	new File(folderPath+"/consumerProperties/QuartzConsumer.java").renameTo(folderPath+"/properties/QuartzConsumer.java")
	new File(folderPath+"/consumerRoutes/QuartzConsumerRoute.java").renameTo(folderPath+"/routes/QuartzConsumerRoute.java")
}

if (varVerificar == 'imap' ) {
	new File(rutaArchivos+"imap.properties").renameTo(rutaResources + "imap.properties")
	new File(folderPath+"/consumerProperties/ImapConsumer.java").renameTo(folderPath+"/properties/ImapConsumer.java")
	new File(folderPath+"/consumerRoutes/ImapConsumerRoute.java").renameTo(folderPath+"/routes/ImapConsumerRoute.java")
}

//Verify the output type supported. 
if (varOutput == 'amq' ) {
	new File(rutaArchivos+"amq_prod_base.properties").renameTo(rutaResources + "amq_prod_base.properties")
	new File(folderPath+"/configuradorProducer/AmqConfigurationProducer.java").renameTo(folderPath+"/configurator/AmqConfigurationProducer.java")
	new File(folderPath+"/producerProperties/AmqProducer.java").renameTo(folderPath+"/properties/AmqProducer.java")
	new File(folderPath+"/producerRoutes/AMQProducerRoute.java").renameTo(folderPath+"/routes/AMQProducerRoute.java")
}

if (varOutput == 'soap' ) {
	new File(rutaArchivos+"soap_base.properties").renameTo(rutaResources + "soap_prod_base.properties")
	new File(rutaArchivos+"GreetingService.wsdl").renameTo(rutaResources + "GreetingService.wsdl")
	new File(folderPath+"/producerProperties/SoapProducer.java").renameTo(folderPath+"/properties/SoapProducer.java")
	new File(folderPath+"/producerRoutes/SoapProducerRoute.java").renameTo(folderPath+"/routes/SoapProducerRoute.java")
	print rutaArchivos
}

if (varOutput == 'sql' ) {
	new File(rutaArchivos+"database_prod_base.properties").renameTo(rutaResources + "database_prod_base.properties")
	new File(folderPath+"/configuradorProducer/DatasourceConfigurationProducer.java").renameTo(folderPath+"/configurator/DatasourceConfigurationProducer.java")
	new File(folderPath+"/producerProperties/DatasourceProducer.java").renameTo(folderPath+"/properties/DatasourceProducer.java")
	new File(folderPath+"/producerRoutes/DatasourceProducerRoute.java").renameTo(folderPath+"/routes/DatasourceProducerRoute.java")
	print rutaArchivos
}

if (varOutput == 'batis' ) {
	new File(rutaArchivos+"database_prod_base.properties").renameTo(rutaResources + "database_prod_base.properties")
	new File(folderPath+"/configuradorProducer/DatasourceConfigurationProducer.java").renameTo(folderPath+"/configurator/DatasourceConfigurationProducer.java")
	new File(folderPath+"/producerProperties/DatasourceProducer.java").renameTo(folderPath+"/properties/DatasourceProducer.java")
	new File(rutaArchivos+"batis.properties").renameTo(rutaResources + "batis.properties")
	new File(rutaArchivos+"OrderMapper.xml").renameTo(rutaResources + "OrderMapper.xml")
	new File(folderPath+"/configuradorProducer/MyBatisConfiguration.java").renameTo(folderPath+"/configurator/MyBatisConfiguration.java")
	new File(folderPath+"/producerProperties/BatisProducer.java").renameTo(folderPath+"/properties/BatisProducer.java")
	new File(folderPath+"/producerRoutes/MyBatisProducerRoute.java").renameTo(folderPath+"/routes/MyBatisProducerRoute.java")
	print rutaArchivos
}

if (varOutput == 'sftp' || varVerificar == 'ftp') {
	new File(rutaArchivos+"sftp_prod_base.properties").renameTo(rutaResources + "sftp_prod_base.properties")
	new File(folderPath+"/producerProperties/SftpProducer.java").renameTo(folderPath+"/properties/SftpProducer.java")
	new File(folderPath+"/producerRoutes/SftpProducerRoute.java").renameTo(folderPath+"/routes/SftpProducerRoute.java")
	print rutaArchivos
}

if (varOutput == 'smtp') {
	new File(rutaArchivos+"smtp_prod_base.properties").renameTo(rutaResources + "smtp_prod_base.properties")
	new File(rutaArchivos+"generic.vm").renameTo(rutaResources + "generic.vm")
	new File(folderPath+"/producerProperties/SmtpProducer.java").renameTo(folderPath+"/properties/SmtpProducer.java")
	new File(folderPath+"/producerRoutes/SmtpProducerRoute.java").renameTo(folderPath+"/routes/SmtpProducerRoute.java")
	new File(folderPath+"/transformationsClass/LoaderTemplatesProcessor.java").renameTo(folderPath+"/transformations/LoaderTemplatesProcessor.java")
	new File(folderPath+"/transformationsClass/AttachmentsProcessor.java").renameTo(folderPath+"/transformations/AttachmentsProcessor.java")
	print rutaArchivos
}

if (varOutput == 'rest' ) {
	new File(rutaArchivos+"rest_prod_base.properties").renameTo(rutaResources + "rest_prod_base.properties")
	new File(folderPath+"/producerProperties/RestProducer.java").renameTo(folderPath+"/properties/RestProducer.java")
	new File(folderPath+"/producerRoutes/RestProducerRoute.java").renameTo(folderPath+"/routes/RestProducerRoute.java")
	print rutaArchivos
}


def dest = folderPath+"/routes"
new File(folderPath+"/transformationRoutes").eachFile(){
	file -> file.renameTo(dest + "/" + file.name)
}
new File(folderPath+"/transformationRoutes").delete()

new File(folderPath+"/consumerProperties").eachFile(){
	file -> file.delete()
}
new File(folderPath+"/consumerRoutes").eachFile(){
	file -> file.delete()
}
new File(folderPath+"/consumerRoutes").delete()

new File(folderPath+"/configuradorConsumer").eachFile(){
	file -> file.delete()
}
new File(folderPath+"/producerProperties").eachFile(){
	file -> file.delete()
}
new File(folderPath+"/producerRoutes").eachFile(){
	file -> file.delete()
}
new File(folderPath+"/producerRoutes").delete()

new File(folderPath+"/configuradorProducer").eachFile(){
	file -> file.delete()
}

new File(folderPath+"/configuradorConsumer").eachFile(){
	file -> file.delete()
}

new File(folderPath+"/mailTransformaciones").eachFile(){
	file -> file.delete()
}

new File(folderPath+"/transformationsClass").eachFile(){
	file -> file.delete()
}

new File(rutaArchivos).eachFile(){
	file -> file.delete()
}

//This part replace the paths inside the application.properties
//to take the path to the project directly.
def file = new File(rutaResources+"application.properties")
def fileText = file.text.replaceAll("--buildDirectory--", rutaResources.replaceAll("\\\\","/"))
    file.write(fileText);
def fileNew = new File(rutaResourcesTest+"application.properties")
def fileTextNew = fileNew.text.replaceAll("--buildDirectory--", rutaResources.replaceAll("\\\\","/"))
    fileNew.write(fileTextNew);


//This part delete all the folders and files we don't use.
new File(folderPath+"/sftpPollStrategyListener/").deleteDir()
new File(rutaArchivos).delete()
new File(folderPath+"/routes/deleteme.java").delete()
new File(folderPath+"/properties/deleteme.java").delete()
new File(folderPath+"/consumerProperties").delete()
new File(folderPath+"/mailTransformaciones").delete()
new File(folderPath+"/transformationRoutes").delete()
new File(folderPath+"/producerProperties").delete()
new File(folderPath+"/configuradorConsumer").delete()
new File(folderPath+"/configuradorProducer").delete()
new File(folderPath+"/configuradorDataConsumer").delete()
new File(folderPath+"/configuradorDataProducer").delete()
new File(folderPath+"/transformationsClass").delete()
