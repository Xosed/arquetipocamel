#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright 2005-2016 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version
 * 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
package ${package}.${artefactIdLower}.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Configuration;

@Configuration
@PropertySource("file:///${CONFIG_LOCATION}/sftp_base.properties")
@ConfigurationProperties(prefix = "${artifactId}.ftp")
public class SftpConsumer {
	
	private String hostName;
	private String port;
	private String user;
	private String passwd;
	private String pathDownload;
	private String regexFileName;
	private String processedDirectory;
	private String errorDirectory;
	private String autoCreate;
	private String jschLoggingLevel;
	private int reconnectAttempts;
	
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getPathDownload() {
		return pathDownload;
	}
	public void setPathDownload(String pathDownload) {
		this.pathDownload = pathDownload;
	}
	public String getRegexFileName() {
		return regexFileName;
	}
	public void setRegexFileName(String regexFileName) {
		this.regexFileName = regexFileName;
	}
	public String getProcessedDirectory() {
		return processedDirectory;
	}
	public void setProcessedDirectory(String processedDirectory) {
		this.processedDirectory = processedDirectory;
	}
	public String getErrorDirectory() {
		return errorDirectory;
	}
	public void setErrorDirectory(String errorDirectory) {
		this.errorDirectory = errorDirectory;
	}
	public String getAutoCreate() {
		return autoCreate;
	}
	public void setAutoCreate(String autoCreate) {
		this.autoCreate = autoCreate;
	}
	public String getJschLoggingLevel() {
		return jschLoggingLevel;
	}
	public void setJschLoggingLevel(String jschLoggingLevel) {
		this.jschLoggingLevel = jschLoggingLevel;
	}
	public int getReconnectAttempts() {
		return reconnectAttempts;
	}
	public void setReconnectAttempts(int reconnectAttempts) {
		this.reconnectAttempts = reconnectAttempts;
	}
}