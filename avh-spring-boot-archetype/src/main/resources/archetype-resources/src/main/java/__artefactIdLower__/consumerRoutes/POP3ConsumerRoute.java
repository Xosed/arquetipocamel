#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright 2005-2016 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version
 * 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
package ${package}.${artefactIdLower}.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ${package}.${artefactIdLower}.properties.POP3Consumer;
import ${package}.${artefactIdLower}.configurator.ConfigurationRoute;

@Component
public class POP3ConsumerRoute extends ConfigurationRoute  {
	
	@Autowired
	private POP3Consumer pop3Config; 

	public void configure() throws Exception {  
		super.configure();
//        context().setStreamCaching(true);  
		String path = pop3Config.getUser() + "@" + pop3Config.getDomain() + "?password=" 
					+ pop3Config.getPasswd() + "&consumer.delay=" + pop3Config.getDelay();
        from("pop3s://"+ path).routeId("${artefactIdLower}_pop3_consumer")
            .setHeader("subject", constant(pop3Config.getSubject()))  
            .choice()  
              .when(header("attachmentName").isEqualTo(pop3Config.getAttach()))  
              .to("direct:transformationRoute")
              .otherwise()  
                .to("mock:destination2")  
            .end();  
      }  
}
