		final RouteDefinition routeConsumer = context.getRouteDefinition("${artefactIdLower}_database_consumer");
		routeConsumer.adviceWith(context, new AdviceWithRouteBuilder() {
			@Override
			public void configure() throws Exception {
				replaceFromWith("direct:sqlConsumerRoute");
				mockEndpointsAndSkip("sql:*");				
			}
		});		