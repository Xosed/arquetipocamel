#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright 2005-2016 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version
 * 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
package ${package}.${artefactIdLower}.transformations;

import org.apache.camel.Consumer;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.spi.PollingConsumerPollStrategy;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component("sftpPollingConsumerPollStrategy")
public class SftpPollingConsumerPollStrategy implements PollingConsumerPollStrategy{
	
	static final Logger logger = Logger.getLogger("${artifactId}Logger");

	@Override
	public boolean begin(Consumer consumer, Endpoint endpoint) {
		return true;
	}

	@Override
	public void commit(Consumer consumer, Endpoint endpoint, int polledMessages) {
	}
	
	@Override
	public boolean rollback(Consumer consumer, Endpoint endpoint, int retryCounter, Exception cause) throws Exception {
		if (cause != null && cause.getClass().getName().equalsIgnoreCase("org.apache.camel.component.file.GenericFileOperationFailedException")) {
			logger.info("Tipo Excepcion: "+cause.getClass().getName());
			logger.info("Mensaje: "+cause.getMessage());
			if (cause.getCause() != null) {
				logger.info("Causa: "+cause.getCause());
			}
			cause.printStackTrace();
			Exchange ex = endpoint.createExchange();
			Exception exception = new Exception("Se ha perdido la conexion al servidor SFTP. Por favor, requerimos de su intervencion lo antes posible. Gracias.", new Throwable("Error de conexion."));
			ex.setProperty(Exchange.EXCEPTION_CAUGHT, exception);
			ex.getMessage().setHeader("mailErrorDescription", "Lost Connection");
			endpoint.getCamelContext().createProducerTemplate().asyncSend("direct:mailNotification", ex);			
		} else {
			logger.info("NORMAL FUNCTION.!!");
		}
		return false;
	}
}
