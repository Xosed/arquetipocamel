package ${package}.${artefactIdLower}.transformations;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.activation.DataHandler;
import javax.mail.util.ByteArrayDataSource;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Procesador que obtiene todos los adjuntos en el body de tipo map y los añade como Attachments al message del exchange
 * El formato de la key de los Attachments es:
 * attachment.1.name = nombre del adjunto
 * attachment.1.mimeType = mimeType \src\main\resources\META-INF\mime.types
 * attachment.1.content = contenido del archivo Array de bytes en formato Base64
 * attachment.n.name
 * attachment.n.mimeType
 * attachment.n.content
 */
@Component("attachmentsProcessor")
public class AttachmentsProcessor {

    private static final Logger logger = LoggerFactory.getLogger("${artifactId}Logger");
    
    public void ejecutar(Exchange exchange) throws Exception {
        Map params = exchange.getIn().getBody(Map.class);
        String patternString = "(attachment\\.)(\\d)(\\.name)";
        Pattern pattern = Pattern.compile(patternString);
        Set set = params.keySet();
        for (Object key : set) {
            String s = String.valueOf(key);
            Matcher matcher = pattern.matcher(s);
            if (matcher.find()) {
                String attachment = matcher.group(2);
                byte[] content = Base64.decodeBase64((byte[]) params.get("attachment." + attachment + ".content"));
                String name = (String) params.get("attachment." + attachment + ".name");
                String mimeType = (String) params.get("attachment." + attachment + ".mimeType");
                DataHandler dataHandler = new DataHandler(new ByteArrayDataSource(content, mimeType));
                exchange.getIn().addAttachment(name, dataHandler);
            }
        }
        logger.info("Attachaments Found: " + exchange.getIn().getAttachmentNames());
    }
}
