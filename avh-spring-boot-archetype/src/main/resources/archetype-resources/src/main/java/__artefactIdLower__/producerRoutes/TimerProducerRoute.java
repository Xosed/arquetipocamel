#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright 2005-2016 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version
 * 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
package ${package}.${artefactIdLower}.routes;

import java.net.ConnectException;
import java.sql.SQLRecoverableException;
import java.sql.SQLSyntaxErrorException;
import java.sql.SQLException;

import org.apache.camel.LoggingLevel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ${package}.${artefactIdLower}.properties.TimerProducer;
import ${package}.${artefactIdLower}.configurator.ConfigurationRoute;
import ${package}.${artefactIdLower}.properties.MailProperties;

@Component
public class TimerProducerRoute extends ConfigurationRoute{

	@Autowired
	private TimerProducer timerConfig;
	
	@Autowired
	private MailProperties mailConfig;
	
    @Override
    public void configure() throws Exception {
    	super.configure();
    	
    	onException(ConnectException.class).handled(true)
		    .maximumRedeliveries(3)
		    .redeliveryDelay(2000)
		    .log(LoggingLevel.ERROR, "TRV-01 El host de destino no ha sido alcanzado presenta errores de conexi�n en la ruta ${routeId}")
		    .log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
		    .log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
		    .log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
		    .setHeader("mailErrorDescription", simple(mailConfig.getErrorConexion().toString()))
		    .to("direct:mailNotification")
		.end();
		
		onException(SQLRecoverableException.class).handled(true)
		    .maximumRedeliveries(3)
		    .redeliveryDelay(3000)
		    .log(LoggingLevel.ERROR, "SQL-01 La transaccion que se que se esta ejecutando presenta errores en la ruta ${routeId}")
		    .log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
		    .log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
		    .log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
		    .setHeader("mailErrorDescription", simple(mailConfig.getErrorConexion().toString()))
		    .to("direct:mailNotification")
		.end();
		
		onException(SQLSyntaxErrorException.class).handled(true)
		    .log(LoggingLevel.ERROR, "SQL-03 La sentencia que se esta ejecutando presenta errores en la ruta ${routeId}")
		    .log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
		    .log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
		    .log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
		    .setHeader("mailErrorDescription", simple(mailConfig.getErrorConexion().toString()))
		    .to("direct:mailNotification")
		.end();
		
		onException(SQLException.class).handled(true)
			.log(LoggingLevel.ERROR, "SQL-02 La transaccion que se esta ejecutando presenta errores en la ruta ${routeId}")
			.log(LoggingLevel.ERROR, "ExceptionClass: ${exchangeProperty.CamelExceptionCaught.class}")
			.log(LoggingLevel.ERROR, "ExceptionClassName: ${exchangeProperty.CamelExceptionCaught.class.name}")
			.log(LoggingLevel.ERROR, "StackTrace: ${exception.stacktrace}")
			.setHeader("mailErrorDescription", simple(mailConfig.getErrorSentenciaSQL().toString()))
			.to("direct:mailNotification")
		.end();
    	
        // A first route generates some orders and queue them in DB
        from("direct:timerProducerRoute")
        	.routeId("${artefactIdLower}_timer_producer")
        	.to("timer:new-order?delay=1s&period={{quickstart.generateOrderPeriod:2s}}")
            .bean("orderService", "generateOrder")
            .to("sql:insert into orders (id, item, amount, description, processed) values " +
                "(:#${body.id} , :#${body.item}, :#${body.amount}, :#${body.description}, false)?" +
                "dataSource=dataSource")
            .log("Inserted new order ${body.id}")
        .end();
    }

}
