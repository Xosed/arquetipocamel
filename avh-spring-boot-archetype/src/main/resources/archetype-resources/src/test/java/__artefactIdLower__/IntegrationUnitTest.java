package ${package}.${artefactIdLower};

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.MockEndpointsAndSkip;
import org.apache.camel.test.spring.UseAdviceWith;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.junit.Ignore;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import java.io.FileReader;
import java.io.Reader;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import ${package}.${artefactIdLower}.IntegrationUnitTest;

@RunWith(CamelSpringBootRunner.class)
@SpringBootTest(classes = IntegrationUnitTest.class)
@UseAdviceWith
@SpringBootApplication
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
#if(${outputTypeSupport} == "amq")	
@MockEndpointsAndSkip("direct:amqNotificationProducerRoute|direct:amqProducerRoute")
#end
#if(${outputTypeSupport} == "sftp" || ${outputTypeSupport} == "ftp")	
@MockEndpointsAndSkip("direct:amqNotificationProducerRoute|direct:sftpProducerRoute")
#end
#if(${outputTypeSupport} == "rest")
@MockEndpointsAndSkip("direct:amqNotificationProducerRoute|direct:restProducerRoute")
#end
#if(${outputTypeSupport} == "sql")	
@MockEndpointsAndSkip("direct:amqNotificationProducerRoute|direct:datasourceProducerRoute")
#end
#if(${outputTypeSupport} == "smtp")	
@MockEndpointsAndSkip("direct:amqNotificationProducerRoute|direct:smtpProducerRoute")
#end
#if(${outputTypeSupport} == "soap")	
@MockEndpointsAndSkip("direct:amqNotificationProducerRoute|direct:soapProducerRoute")
#end
#if(${outputTypeSupport} == "batis")	
@MockEndpointsAndSkip("direct:amqNotificationProducerRoute|direct:batisProducerRoute")
#end
public class IntegrationUnitTest {

	@Autowired
	private ProducerTemplate template;
	
	@Autowired
	private CamelContext context;

	@EndpointInject(uri="mock:direct:amqNotificationProducerRoute")
	MockEndpoint mockNotificationEndpoint;

#if(${outputTypeSupport} == "amq")	
#parse ( "test/MockProducerAMQ.txt")
#end
#if(${outputTypeSupport} == "sftp" || ${outputTypeSupport} == "ftp")	
#parse ( "test/MockProducerSFTP.txt")
#end
#if(${outputTypeSupport} == "rest")	
#parse ( "test/MockProducerRest.txt")
#end
#if(${outputTypeSupport} == "sql")	
#parse ( "test/MockProducerSQL.txt")
#end
#if(${outputTypeSupport} == "smtp")	
#parse ( "test/MockProducerSMTP.txt")
#end
#if(${outputTypeSupport} == "soap")	
#parse ( "test/MockProducerSOAP.txt")
#end
#if(${outputTypeSupport} == "batis")	
#parse ( "test/MockProducerBatis.txt")
#end

	
	private final static String REQUEST_SUCCESS_MESSAGE = "src/test/resources/xmlIn/request_success.txt";
	private final static String REQUEST_FAILED_MESSAGE = "src/test/resources/xmlIn/request_failed.txt";
	private final static String RESPONSE_DB = "src/test/resources/xmlIn/reponse_db.csv";
	private String expectedSuccessMessage = "src/test/resources/expectedMessages/expected_success.txt";
	private String expectedFailedMessage = "src/test/resources/expectedMessages/expected_failed.txt";

	@Before
	public void ruoteConfiguration() throws Exception {

#if(${incomingTypeSupport} == "amq")	
#parse ( "test/MockConsumerAMQ.txt")
#end
#if(${incomingTypeSupport} == "sql")	
#parse ( "test/MockConsumerSQL.txt")
#end
#if(${incomingTypeSupport} == "sftp" || ${incomingTypeSupport} == "ftp")	
#parse ( "test/MockConsumerFTP.txt")
#end
#if(${incomingTypeSupport} == "pop3")	
#parse ( "test/MockConsumerPop3.txt")
#end
#if(${incomingTypeSupport} == "quartz")	
#parse ( "test/MockConsumerQuartz.txt")
#end
#if(${incomingTypeSupport} == "imap")	
#parse ( "test/MockConsumerImap.txt")
#end


	}

	@Test
	public void successRequestTransformationTest() throws Exception 
	{

		context.start();
		assertTrue(context.getStatus().isStarted());
	
		File expectedSuccessFile = new File(REQUEST_SUCCESS_MESSAGE);
#if(${incomingTypeSupport} == "amq")	
		template.requestBody("direct:amqConsumerRoute", expectedSuccessFile);
#end
#if(${incomingTypeSupport} == "sql")	
		template.requestBody("direct:sqlConsumerRoute", expectedSuccessFile);
#end
#if(${incomingTypeSupport} == "sftp" || ${incomingTypeSupport} == "ftp")	
		template.requestBodyAndHeader("direct:ftpConsumerRoute", expectedSuccessFile, "attachmentName", "testFile");
#end
#if(${incomingTypeSupport} == "pop3")	
		template.requestBody("direct:pop3ConsumerRoute", expectedSuccessFile);
#end
#if(${incomingTypeSupport} == "quartz")	
		template.requestBody("direct:quartz2ConsumerRoute", expectedSuccessFile);
#end
#if(${incomingTypeSupport} == "imap")	
		template.requestBodyAndHeader("direct:imapConsumerRoute", expectedSuccessFile, "attachmentName", "testFile");
#end
#if(${incomingTypeSupport} == "rest")	
		template.requestBody("direct:transformationRoute", expectedSuccessFile);
#end
		File compareMessages = new File(expectedSuccessMessage);
		String expectedMessage = context.getTypeConverter().convertTo(String.class, compareMessages);
		mockProducerEndpoint.message(0).body(String.class).equals(expectedMessage);
		mockProducerEndpoint.assertIsSatisfied();
		
	}

	@Test
	public void failedRequestTransformationTest() throws Exception 
	{
		
		context.start();
		assertTrue(context.getStatus().isStarted());
		mockNotificationEndpoint.expectedMessageCount(1);

		File expectedFailedFile = new File(REQUEST_FAILED_MESSAGE);
#if(${incomingTypeSupport} == "amq")	
		template.requestBody("direct:amqConsumerRoute", expectedFailedFile);
#end
#if(${incomingTypeSupport} == "sql")	
		template.requestBody("direct:sqlConsumerRoute", expectedFailedFile);
#end
#if(${incomingTypeSupport} == "sftp" || ${incomingTypeSupport} == "ftp")	
		template.requestBodyAndHeader("direct:ftpConsumerRoute", expectedFailedFile, "attachmentName", "testFile");
#end
#if(${incomingTypeSupport} == "pop3")	
		template.requestBody("direct:pop3ConsumerRoute", expectedFailedFile);
#end
#if(${incomingTypeSupport} == "quartz")	
		template.requestBody("direct:quartz2ConsumerRoute", expectedFailedFile);
#end
#if(${incomingTypeSupport} == "imap")	
		template.requestBodyAndHeader("direct:imapConsumerRoute", expectedFailedFile, "attachmentName", "testFile");
#end
#if(${incomingTypeSupport} == "rest")	
		template.requestBody("direct:transformationRoute", expectedFailedFile);
#end

		File compareMessages = new File(expectedFailedMessage);
		String expectedMessage = context.getTypeConverter().convertTo(String.class, compareMessages);
		mockNotificationEndpoint.message(0).body().toString().contains(expectedMessage);
		mockNotificationEndpoint.assertIsSatisfied();		
		mockProducerEndpoint.assertIsSatisfied();				
	}
	
	@Ignore
	@Test
	public void successdRequestEnrichTest() throws Exception 
	{
		final RouteDefinition estadoadm_amq_consumer = context.getRouteDefinition("estadoadm_amq_consumer");
		estadoadm_amq_consumer.adviceWith(context, new AdviceWithRouteBuilder() {
			@Override
			public void configure() throws Exception {				
				weaveById("enrich").replace().process((e) -> {
					Reader in = new FileReader(RESPONSE_DB);
					List<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(in).getRecords();
					List<Map<String, String>> rows = records.stream().map(r -> r.toMap()).collect(Collectors.toList());
                    e.getIn().setBody(rows);  
				});
			}
		});
		
		context.start();
		assertTrue(context.getStatus().isStarted());
		
		File expectedSuccessFile = new File(REQUEST_SUCCESS_MESSAGE);
#if(${incomingTypeSupport} == "amq")	
		template.requestBody("direct:amqConsumerRoute", expectedSuccessFile);
#end
#if(${incomingTypeSupport} == "sql")	
		template.requestBody("direct:sqlConsumerRoute", expectedSuccessFile);
#end
#if(${incomingTypeSupport} == "sftp" || ${incomingTypeSupport} == "ftp")	
		template.requestBodyAndHeader("direct:ftpConsumerRoute", expectedSuccessFile, "attachmentName", "testFile");
#end
#if(${incomingTypeSupport} == "pop3")	
		template.requestBody("direct:pop3ConsumerRoute", expectedSuccessFile);
#end
#if(${incomingTypeSupport} == "quartz")	
		template.requestBody("direct:quartz2ConsumerRoute", expectedSuccessFile);
#end
#if(${incomingTypeSupport} == "imap")	
		template.requestBodyAndHeader("direct:imapConsumerRoute", expectedSuccessFile, "attachmentName", "testFile");
#end
#if(${incomingTypeSupport} == "rest")	
		template.requestBody("direct:transformationRoute", expectedSuccessFile);
#end
		File compareMessages = new File(expectedSuccessMessage);
		String expectedMessage = context.getTypeConverter().convertTo(String.class, compareMessages);
		mockProducerEndpoint.message(0).body(String.class).equals(expectedMessage);
		mockProducerEndpoint.assertIsSatisfied();		
	}
}
