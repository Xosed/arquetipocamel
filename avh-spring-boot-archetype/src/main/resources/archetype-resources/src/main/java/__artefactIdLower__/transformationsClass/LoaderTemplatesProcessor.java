package ${package}.${artefactIdLower}.transformations;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import ${package}.${artefactIdLower}.properties.SmtpProducer;

/**
 * Bean que se encarga de cargar las templates usando el URL Handler profile de fabric
 */
@Component("cargaPlantillas")
public class LoaderTemplatesProcessor {
	
	@Value("${CONFIG_LOCATION}")
    private String configurationLocation;
	
	@Autowired
	private SmtpProducer templateConfig;

    private static final Logger logger = LoggerFactory.getLogger("${artifactId}Logger");

    private Map<String, URL> cacheMap;

	/**
     * Instantiates a new Loader templates processor.
     *
     * @param strTemplates listado de plantillas separadas por coma
     * @throws Exception the exception
     */
//    public LoaderTemplatesProcessor() throws Exception {
//    	System.out.println("Entré al procesador básico");
//    	if(templateConfig == null)
//    	{
//    		System.out.println("VARIABLE NULA!!!!!!!!!!");
//    	}
//    	else {
//	        String[] arrTemplates = templateConfig.getName().split(",");
//	        cacheMap = new HashMap<>();
//	        for (String templateName : arrTemplates) {
//	            cacheMap.put(templateName, new URL("classpath:" + templateName + ".vm"));
//	        }
//    	}
//    }

    /**
     * Carga una plantilla en el header CamelVelocityTemplate
     *
     * @param ex exchange con el header template de la plantilla a cargar
     */
    public void loadTemplate(Exchange ex) throws Exception {
        cacheMap = new HashMap<>();
        logger.warn("\n\n"+ex.getContext().getClassResolver().getClass().getName()+"\n\n");
       	cacheMap.put(templateConfig.getTemplate(), new URL("file:///"+ configurationLocation+ "/" +templateConfig.getTemplate()+".vm"));
       	logger.warn("\n\n"+"CACHEA PLANTILLA: "+templateConfig.getTemplate()+"\n\n");
        
        String nameTemplate = ex.getIn().getHeader("template", String.class);
        logger.warn("\n\nCABECERO TEMPLATE: "+nameTemplate+"\n\n");
        URL urlTemplate = cacheMap.get(nameTemplate);
        if (urlTemplate == null) {
            logger.warn("La plantilla " + nameTemplate + "no ha sido encontrada, cargando generic");
            urlTemplate = cacheMap.get("generic");
        }
        else {
        	logger.warn("\n\nNO ES NULO\n\n");
        }
        ex.getOut().setHeaders(ex.getIn().getHeaders());
        ex.getOut().setBody(ex.getIn().getBody());
        ex.getOut().setHeader("CamelVelocityTemplate", urlTemplate);
        logger.warn("\n\nCamelVelocityTemplate: "+ex.getOut().getHeader("CamelVelocityTemplate")+"\n\n");
    }
    
}
